package u02

object HelloWorld extends App {
  // ES 2
  println("Hello, Scala!")

  // ES 3.a
  val parity: Int => String = {
      case x if (x%2) == 0 => "even"
      case x if (x%2) != 0 => "odd"
  }

  println(parity(3))

  def parity_method(x: Int) = x%2 match {
    case 0 => "even"
    case _ => "odd"
  }

  println(parity_method(7))

  // ES 3.b
  val neg: (String => Boolean) => (String => Boolean) = p => (s => (!p(s)))

  val empty: String => Boolean = _==""

  println(empty("ciao"))

  val not_empty = neg(empty)
  println(not_empty("ciao"))

  def neg_method(f: String => Boolean): (String => Boolean) = a => !f(a)
  val a = neg_method(empty)

  println(a("ciao"))

  // ES 3.c
  def neg_method_generic[A](f: (A => Boolean)): (A => Boolean) = a => !f(a)

  val b = neg_method_generic(empty)
  println(b("ciao"))

  val even_checker: Int => Boolean = _%2 == 0

  val c = neg_method_generic(even_checker)
  println(c(3))

  // ES 4

}

